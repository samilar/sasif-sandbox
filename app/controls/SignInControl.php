<?php

namespace app\controls;



use SaSiF\SaSiF\Authenticator;
use SaSiF\SaSiF\Container;
use SaSiF\SaSiF\Control;
use SaSiF\SaSiF\Database;
use SaSiF\SaSiF\dto\ConfigDTO;
use SaSiF\SaSiF\Form;
use SaSiF\SaSiF\interfaces\IForm;

class SignInControl extends Control
{

    public function __construct(Container $container, Database $database, Authenticator $authenticator, ConfigDTO $configDTO)
    {
        parent::__construct($container, $database, $authenticator, $configDTO);
    }

    public function componentSignInForm() {
        $form = new Form();
        $form->addTextInput('login', 'Login');
        $form->addPasswordInput('password', 'Heslo');
        $form->addSubmit('Přihlásit');
        $form->setProcessMethod('processSignIn');
        return $form;
    }

    public function actionSignIn(){}

    public function renderSignIn()
    {
        if ($this->authenticator->isLogged()) {
            $this->redirect('dashboard/dashboard');
        }
    }

    public function processSignIn(IForm $form)
    {
        if ($this->authenticator->processSignInForm($form)) {
            $this->flashMessage('Byl/a si přihlášen/a.', 'success');
            $this->redirect('dashboard/dashboard');
        }
    }
}