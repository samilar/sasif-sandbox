<?php

namespace app\controls;

use app\model\CronRunModel;
use vendor\SaSiF\Authenticator;
use vendor\SaSiF\Container;
use vendor\SaSiF\Control;
use vendor\SaSiF\Database;
use vendor\SaSiF\dto\ConfigDTO;
use vendor\SaSiF\Form;

class TestControl extends Control
{

    private $cronRunModel;

    public $a;
    public $b;
    public $c;
    public $d;
    public $e;
    public $f;
    public $g;

    public function __construct(Container $container, Database $database, Authenticator $authenticator, ConfigDTO $configDTO)
    {
        parent::__construct($container, $database, $authenticator, $configDTO);
        $this->cronRunModel = new CronRunModel();
        $this->a = $database;
        $this->b = $authenticator;
        $this->c = $configDTO;
        $cls = new \stdClass();
        $cls->test1 = [
            'a',
            'b',
            'c',
            'd'
        ];
        $cls->test2 = new \stdClass();
        $cls->test2->test2_1 = [
            'e',
            'f',
            'g',
            'h'
        ];
        $this->d = $cls;
        $this->e = 10;
        $this->f = 'abcd';
        $this->g = false;
    }

    public function componentTestForm() {
        $form = new Form();

        $form->addTextInput('name', 'Name: ');

        $form->addTextInput('surname', 'Surname: ');

        $form->addCheckbox('checkbox1', 'Test checkbox not checked');
        $form->addCheckbox('checkbox2', 'Test checkbox checked', true);

        $multiCheckboxValues = ['ABC', 'DEF(sel)', 'GHI', 'JKL(sel)', 'MNO'];
        $multiCheckboxChecked = ['DEF(sel)', 'JKL(sel)'];
        $form->addMultiCheckbox('multicheckbox', $multiCheckboxValues, $multiCheckboxChecked);

        $radios1 = ['AB', 'CD', 'EF', 'GH'];
        $form->addRadio('radio1', $radios1);

        $radios2 = ['A_B', 'C_D', 'E_F(sel)', 'G_H'];
        $form->addRadio('radio2', $radios2, 'E_F(sel)');

        $select = [
            'jedna' => 'Jedna',
            'dva' => 'Dva(sel)',
            'tri' => 'Tri'
        ];
        $form->addSelect('select1', 'Testing: ', $select , 'dva');

        $form->addTextArea('area1', 'Aréa: ', 'Text v arée :)');

        $form->addSubmit('Brekeke');
        $form->setProcessMethod('processAddPlaylist');
        return $form;
    }

    public function actionTest()
    {
//        $cls = new \stdClass();
//        $cls->test1 = [
//            'a',
//            'b',
//            'c',
//            'd'
//        ];
//        $cls->test2 = new \stdClass();
//        $cls->test2->test2_1 = [
//            'e',
//            'f',
//            'g',
//            'h'
//        ];
//        echo '<pre>';
//        print_r($cls);
//        var_dump($cls);
//        echo '</pre>';
//        dump($cls);
//        var_dump($this);
//        dump($this);
        dump(new Tralala());
        dump(new Tralala(), true);
//        $query = $this->cronRunModel->getByColumns(
//            [
//                'cmd',
//                'response  >=?  '
//            ],
//            [
//                'howno 8',
//                '1'
//            ],
//            'XOR'
//        );
//        dump($query);
//        dump(count($query));
//        dump($this->cronRunModel->saveValuesFromArray([
//            'cmd' => 'howno ' . rand(1,20),
//            'response' => '1',
//            'playlist_id' => 7,
//            'created' => new DateTime()
//        ]));
//        dump($this->cronRunModel->getByColumn('created', '2016-06-18 17:43:03'));
//        dump($this->cronRunModel->getAll());
//        dump($this->cronRunModel->getById(1));
//        dump($this->cronRunModel->getByIds([
//            1,
//            2,
//            4
//        ]));


//        /** @var IInput[] $radio */
//        $radio = $this->getComponent('testForm')->fields['radio1'];
//        foreach ($radio as $item) {
//            if ($item->getValue() == 'EF') {
//                $item->setChecked(true);
//            }
//        }
//        $radio[1]->setChecked(true);
    }

    public function renderTest()
    {
    }

}

// testing case for dumping class/classes
class Tralala {

    public $public1 = 1;
    public $public2 = 'asdf';
    public $public3;
    public $public4;

    protected $protected1 = false;
    private $private1 = [];

    public function __construct() {
        $this->public3 = new Tralala2();
        $this->public4 = new Tralala4();
        for($i=0;$i<=5;$i++) {
            $this->private1[] = [$i=>[rand(1,5)]];
        }
    }
}

class Tralala2 {

    public $publicVar1 = 1;
    public $publicVar2 = 'asdf';
    public $publicVar3;

    protected $protectedVar = false;
    private $privateVar = [];

    public function __construct() {
        $this->publicVar3 = new Tralala3();
        for($i=0;$i<=5;$i++) {
            $this->privateVar[] = [$i=>[rand(1,5)]];
        }
    }
}

class Tralala3 {

    private $privateObjects = [];
    private $privateArrays = [];

    public function __construct() {
        for($i=0; $i<=5; $i++) {
            $this->privateObjects[] = new \stdClass();
            $this->privateArrays[] = [
                $i => [rand(1,5)]
            ];
        }
    }
}

class Tralala4 {

    protected $privateObjects = [];

    public function __construct() {
        $this->privateObjects[] = new Tralala3();
        $this->privateObjects[] = new Tralala3();
        $this->privateObjects[] = new Tralala3();
    }
}
