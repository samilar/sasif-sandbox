<?php
namespace app\model;

use vendor\SaSiF\model\BaseModel;

class CronRunModel extends BaseModel
{
    static $table_name = 'cron_run';

    public function insertLog() {
//        $this->insert([
//            'playlist_id' => 22,
//            'response' => 'response ' . rand(0,100),
//            'cmd' => md5(microtime()),
//            'created' => 'NOW()'
//        ]);
    }

    public function updateLog() {
        $data = [
            'id' => 1,
            'playlist_id' => 22,
            'response' => 'new response ' . rand(0,100),
            'cmd' => 'new ' . md5(microtime()),
            'created' => 'NOW()'
        ];
//        $this->update($data);
    }

    /**
     * @param $playlist_id
     * @param $response
     * @param $cmd
     * @return mixed
     */
    public function saveCronRun($playlist_id, $response, $cmd)
    {
        $query = $this->query(
            'INSERT INTO cron_run (playlist_id, response, cmd, created) VALUES(?,?, ?, NOW())',
            [$playlist_id, $response, $cmd]
        );
        return $query->execute();
    }
}