<html>
<head>
    <title>SAmilar SImple Framework Sandbox</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="/www/css/main.css" rel="stylesheet"/>

    <script src="/www/js/jquery/jquery-2.2.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/www/js/identityToggle.js"></script>
    <script src="/www/js/flashMessages.js"></script>

</head>
<body>
<div class="container">
    {if !empty($this->template->flashes)}
        {foreach $this->template->flashes as $flash}
            <div class="alert alert-{$flash->code}">
                {$flash->message}
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        {/foreach}
    {/if}
    {if $this->authenticator->isLogged()}
        <header id="headerBox">
            <nav id="menu">
                {include "menu/menu"}
            </nav>
        </header>
    {/if}
    <main id="mainBox">
        {#content#}
    </main>
</div>
</body>
</html>