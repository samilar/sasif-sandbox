<ul>
    <li>
        <a href="{link test:test}" class="btn btn-primary">Přehled</a>
    </li>
    <li>
        <a href="{link playlist:addPlaylist}" class="btn btn-primary">Přidat playlist</a>
    </li>
    <li>
        <a href="{link user:settings}" class="btn btn-primary">Nastavení</a>
    </li>
    <li>
        <a href="{link user:logout}" class="btn btn-danger">Odhlásit</a>
    </li>
</ul>
{*{if $this->authenticator->getIdentity()->id == 1}*}
{*<div id="identity">*}
{*<span id="identityToggle">{$this->authenticator->getIdentity()->login}<i class="fa fa-arrow-circle-down"></i></span>*}
{*<pre id="identityDump">{dump $this->authenticator->getIdentity()}</pre>*}
{*</div>*}
{*{/if}*}