<?php
require_once 'shortcuts.php';

spl_autoload_register(function ($className) {
        $class = str_replace('\\', '/', $className);
        $class = __DIR__ . '/' . $class . '.php';
        if (file_exists($class)) {
            include $class;
        }
        else {
            throw new Exception("Unable to load $className ! <pre>" . print_r(debug_backtrace(), true) . "</pre>");
        }
});