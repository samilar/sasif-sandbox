var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');

var lessDir = 'www/css/less';
var targetCSSDir = 'www/css/';

gulp.task('css', function () {
    return gulp.src(lessDir + '/*.less')
        .pipe(less({ style: 'compressed' }).on('error', gutil.log))
        .pipe(gulp.dest(targetCSSDir))
});

gulp.task('watch', function () {
    gulp.watch(lessDir + '/*.less', ['css']);
});

gulp.task('default', ['css', 'watch']);