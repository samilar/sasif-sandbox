<?php

    use SaSiF\SaSiF\Debugger;

    function dump($var, $backtrace = false)
{
    Debugger::dump($var, $backtrace);
}
