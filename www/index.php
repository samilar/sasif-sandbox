<?php
    session_start();

    include '../vendor/autoload.php';
    include '../autoloader.php';

    $application = new \SaSiF\SaSiF\Application();

    $application->run();