$(function () {
    setInterval(function () {
        var flashMessage = $('.alert');
        if (flashMessage.is(':visible')) {
            flashMessage.fadeOut(2500);
        } else {
            flashMessage.remove();
        }
    }, 5000);
});