$(function() {

    $('span#identityToggle').click(function() {
        if ($('pre#identityDump').is(':visible') === true) {
            $('span#identityToggle i.fa').removeClass('fa-arrow-circle-up');
            $('span#identityToggle i.fa').addClass('fa-arrow-circle-down');
            $('pre#identityDump').slideUp();
        } else {
            $('span#identityToggle i.fa').removeClass('fa-arrow-circle-down');
            $('span#identityToggle i.fa').addClass('fa-arrow-circle-up');
            $('pre#identityDump').slideDown();
        }
    });

});